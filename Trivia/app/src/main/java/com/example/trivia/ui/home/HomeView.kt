package com.example.trivia.ui.home

interface HomeView {

    data class CategoryItem(
        val id: Int,
        val title: String,
        var isChecked: Boolean = false,
        val onCheckedListener: (Int) -> Unit
    )

    fun setCategoryItems(categoryItems: List<CategoryItem>)
    fun setStartButtonEnabled()
    fun navigateToGame(id: Int)

}
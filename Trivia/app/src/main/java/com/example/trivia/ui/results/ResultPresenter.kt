package com.example.trivia.ui.results

import android.content.Context
import com.example.trivia.MAX_QUESTIONS
import com.example.trivia.R
import com.example.trivia.ui.results.ResultView.Results

class ResultPresenter(context: Context, private val view: ResultView?, score: Int
) {
    init {
        val scoreText = context.getString(R.string.result_score, score, MAX_QUESTIONS)

        val scoreDescription = when {
            score < 5 -> {
                R.string.result_message_lowScore
            }
            score in 5..7 -> {
                R.string.result_message_mediumScore
            }
            else -> {
                R.string.result_message_highScore
            }
        }

        view?.setResult(
            Results(
                score = scoreText,
                scoreDescription = context.getString(scoreDescription)
            )
        )
    }

    fun onPlayAgainClicked() {
        view?.navigateToHome()
    }

}
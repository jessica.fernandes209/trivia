package com.example.trivia.ui.game

interface GameView {

    data class QuestionItem(
        val id: Int,
        val questionNumber: String,
        val question: String,
        val answers: List<String>,
        val correctAnswer: String,
        val isCorrect: Boolean = false,
        val showResults: Boolean = false,
        val answerPicked: String = "",
        var onAnswerSelectedCallback: ((Int, String) -> Unit)? = null
    )

    fun setQuestionItems(list: List<QuestionItem>)
    fun updateScore(score: String)
    fun navigateToNextQuestion()
    fun navigateToResults(totalScore: Int)

}
package com.example.trivia.data.source.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface PublicApi {

    /**
     * Get a list of categories
     */
    @GET("api_category.php")
    fun getCategories(): Single<CategoryResponse>

    @GET("api.php")
    fun getQuestions(
        @Query("amount") amount: Int,
        @Query("category") id: Int
    ): Single<QuestionResponse>

    companion object{
        fun create(okHttpClient: OkHttpClient, moshi: Moshi, baseUrl: String): PublicApi {
            val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .baseUrl(baseUrl)
                .build()
            return retrofit.create(PublicApi::class.java)
        }
    }
}

@JsonClass(generateAdapter = true)
data class CategoryResponse(
    @Json(name = "trivia_categories") val trivia_categories: List<Category>
)

@JsonClass(generateAdapter = true)
data class Category(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String
)

@JsonClass(generateAdapter = true)
data class QuestionResponse(
    @Json(name = "response_code") val response_code: Int,
    @Json(name = "results") val results: List<Question>
)

@JsonClass(generateAdapter = true)
data class Question(
    @Json(name = "category") val category: String,
    @Json(name = "type") val type: String,
    @Json(name = "difficulty") val difficulty: String,
    @Json(name = "question") val question: String,
    @Json(name = "correct_answer") val correct_answer: String,
    @Json(name = "incorrect_answers") val incorrect_answers: List<String>
)




package com.example.trivia.ui.game

import android.content.res.Resources
import android.text.Html
import android.util.Log
import com.example.trivia.MAX_QUESTIONS
import com.example.trivia.R
import com.example.trivia.data.repository.GameRepository
import com.example.trivia.ui.game.GameView.QuestionItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.*
import kotlin.concurrent.schedule

class GamePresenter(
    private val res: Resources,
    private val view: GameView?,
    gameRepository: GameRepository,
    id: Int) {

    companion object {
        const val SWIPE_TIME_MILLISECONDS = 5000L
    }

    private var disposable: Disposable? = null
    private var questionItems = mutableListOf<QuestionItem>()
    private var totalScore: Int = 0

    init {
        disposable = gameRepository.getQuestions(MAX_QUESTIONS, id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val responseCode = it.response_code
                val questions = it.results

                if (responseCode == 0 && questions.isNotEmpty()) {
                    questionItems.addAll(questions.mapIndexed { index, question ->
                        // Merge answers and stuffled them
                        val id = index + 1
                        // TODO: Extract decode to model layer
                        val questionDecoded = Html.fromHtml(question.question, Html.FROM_HTML_MODE_LEGACY)
                        val answers = (question.incorrect_answers + question.correct_answer).shuffled()
                        val answersDecoded = answers.map { answer -> Html.fromHtml(answer, Html.FROM_HTML_MODE_LEGACY).toString() }
                        QuestionItem(
                            id = id,
                            questionNumber = res.getString(R.string.game_questionNumber, id),
                            question = questionDecoded.toString(),
                            answers = answersDecoded,
                            correctAnswer = question.correct_answer,
                            onAnswerSelectedCallback = this::onAnswerSelectedCallback
                        )
                    })
                    view?.setQuestionItems(questionItems)
                    view?.updateScore(res.getString(R.string.game_score, totalScore))
                }
            },{
                // TODO: Error handler
                Log.d("GamePresenter", it.message)
            })
    }

    private fun onAnswerSelectedCallback(id: Int, answerSelected: String) {

        val question = questionItems.first { it.id == id }
        val indexElement = questionItems.indexOf(question)

        val answerIsCorrect = question.correctAnswer == answerSelected
        if (answerIsCorrect) totalScore += 1

        val questionSelectedUpdated = QuestionItem(
            id = question.id,
            questionNumber = question.questionNumber,
            question = question.question,
            answers = question.answers,
            correctAnswer = question.correctAnswer,
            isCorrect = answerIsCorrect,
            showResults = true,
            answerPicked = answerSelected,
            onAnswerSelectedCallback = null
        )

        questionItems[indexElement] = questionSelectedUpdated
        if (indexElement > 0) {
            questionItems.removeAt(indexElement - 1)
        }

        view?.setQuestionItems(questionItems)
        view?.updateScore(res.getString(R.string.game_score, totalScore))

        Timer("GamePresenter", false).schedule(SWIPE_TIME_MILLISECONDS) {
            if (question.id == MAX_QUESTIONS) {
                view?.navigateToResults(totalScore)
                disposable?.dispose()
            }
            else {
                view?.navigateToNextQuestion()
            }
        }
    }

}
package com.example.trivia.ui.results

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.trivia.R


class ResultFragment : Fragment(), ResultView {

    private var presenter: ResultPresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val score = ResultFragmentArgs.fromBundle(arguments!!).score

        presenter = ResultPresenter(view.context, this, score)

        view.findViewById<Button>(R.id.result_playAgainPrimary).setOnClickListener {
            presenter?.onPlayAgainClicked()
        }
    }

    override fun navigateToHome() {
        findNavController().navigate(ResultFragmentDirections.actionResultFragmentToHomeFragment())
    }

    override fun setResult(results: ResultView.Results) {
        view?.findViewById<TextView>(R.id.result_scoreText)?.text = results.score
        view?.findViewById<TextView>(R.id.result_descriptionText)?.text = results.scoreDescription
    }

}

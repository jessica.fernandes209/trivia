package com.example.trivia.data.source.network


import com.squareup.moshi.Moshi
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class ApiClient {
    companion object {
        const val BASE_URL: String = "https://opentdb.com/"
    }

    private val httpClient: OkHttpClient by lazy {
        OkHttpClient.Builder().apply {
            addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }.build()
    }

    private val moshi: Moshi by lazy {
        Moshi.Builder()
            .build()
    }

    private val publicApi: PublicApi by lazy {
        PublicApi.create(httpClient, moshi, BASE_URL)
    }

    fun getCategories(): Single<CategoryResponse> = publicApi.getCategories()
    fun getQuestions(amount: Int, id: Int): Single<QuestionResponse> = publicApi.getQuestions(amount, id)

}



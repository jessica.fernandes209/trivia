package com.example.trivia.ui.results

interface ResultView {

    data class Results(
        val score: String,
        val scoreDescription: String
    )

    fun setResult(results: Results)
    fun navigateToHome()

}
package com.example.trivia

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }

    override fun onBackPressed() {
        // Disabled hardware back button
        // TODO: Implement a better architecture to do that inside the fragments and also allow back button close the app
        //  if user it's in a root view
    }

}

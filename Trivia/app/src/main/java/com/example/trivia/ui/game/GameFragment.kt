package com.example.trivia.ui.game

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.trivia.R
import com.example.trivia.data.repository.GameRepository
import kotlinx.android.synthetic.main.view_question_page.view.*


class GameFragment : Fragment(), GameView {

    private var presenter: GamePresenter? = null
    private var viewPager: ViewPager2? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.view_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val selectedCategoryId = GameFragmentArgs.fromBundle(arguments!!).id

        viewPager = view.findViewById(R.id.game_viewPager)
        viewPager?.adapter = QuestionViewPagerAdapter(emptyList())
        viewPager?.isUserInputEnabled = false

        presenter = GamePresenter(
            view.context.resources,
            this,
            GameRepository(),
            selectedCategoryId
        )
    }

    override fun setQuestionItems(list: List<GameView.QuestionItem>) {
        viewPager?.adapter = QuestionViewPagerAdapter(list)
        viewPager?.adapter?.notifyDataSetChanged()
    }

    override fun updateScore(score: String) {
        view?.findViewById<TextView>(R.id.game_scoreText)?.text = score
    }

    override fun navigateToNextQuestion() {
        val nextItem = viewPager?.currentItem?.plus(1)!!
        viewPager?.setCurrentItem(nextItem, true)
    }

    override fun navigateToResults(totalScore: Int) {
        findNavController().navigate(GameFragmentDirections.actionGameFragmentToResultFragment(totalScore))
    }

    class QuestionViewPagerAdapter(private val questions: List<GameView.QuestionItem>) :
        RecyclerView.Adapter<QuestionViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            QuestionViewHolder(parent)

        override fun getItemCount() = questions.size

        override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
            holder.setData(questions[position])
        }
    }

    class QuestionViewHolder(private val parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_question_page, parent, false)
    ) {
        fun setData(item: GameView.QuestionItem): QuestionViewHolder {
            val context = parent.context

            itemView.questionPage_numberText.text = item.questionNumber
            itemView.questionPage_questionText.text = item.question

            itemView.questionPage_answersContainer.removeAllViews()
            item.answers.forEach {
                val answer = it
                val padding16 = context.resources.getDimensionPixelSize(R.dimen.padding_16)

                // Creating views dynamically because the quantity of answers is random
                val textView = TextView(context)

                val params = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(0, 0, 0, padding16)
                // TODO: Create a style
                textView.layoutParams = params
                textView.text = answer
                textView.textSize = 24f
                textView.textAlignment = View.TEXT_ALIGNMENT_CENTER
                textView.setTextAppearance(R.style.TextAppearance_AppCompat_Headline)
                textView.setPadding(0, padding16, 0, padding16)
                textView.setBackgroundColor(context.getColor(R.color.colorPrimaryLight))
                textView.setOnClickListener {
                    item.onAnswerSelectedCallback?.invoke(
                        item.id,
                        answer
                    )
                }

                if (item.showResults) {
                    when (answer) {
                        item.correctAnswer -> textView.setBackgroundColor(context.getColor(R.color.correctAnswer))
                        item.answerPicked -> textView.setBackgroundColor(context.getColor(R.color.incorrectAnswer))
                    }
                }

                // Update score
                itemView.questionPage_answersContainer?.addView(textView)
            }

            return this
        }
    }

}

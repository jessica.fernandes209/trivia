package com.example.trivia.ui.home

import android.util.Log
import com.example.trivia.data.repository.GameRepository
import com.example.trivia.ui.home.HomeView.CategoryItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class HomePresenter(
    private val view: HomeView?,
    gameRepository: GameRepository
) {
    private var selectedCategoryId : Int = 0
    private var categoryItems: List<CategoryItem>? = null
    private var disposable: Disposable? = null

    init {
        disposable = gameRepository.getCategories()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                categoryItems = it.trivia_categories.map { category ->
                    CategoryItem(
                        id = category.id,
                        title = category.name,
                        onCheckedListener = this::setCategorySelected
                    )
                }
                view?.setCategoryItems(categoryItems ?: emptyList())
            }, {
                // TODO: Error handler
                Log.d("HomePresenter - Error: ", it.message)
            })
    }

    private fun setCategorySelected(id: Int) {
        if (selectedCategoryId != 0) {
            val newCategoryItems = categoryItems?.map {
                    CategoryItem(
                        id = it.id,
                        title = it.title,
                        isChecked = it.id == id,
                        onCheckedListener = this::setCategorySelected
                    )
            }
            view?.setCategoryItems(newCategoryItems ?: emptyList() )
        }
        else {
            // First time an item was added to the list
            view?.setStartButtonEnabled()
        }
        selectedCategoryId = id
    }

    fun onStartButtonClicked() {
        view?.navigateToGame(selectedCategoryId)
        disposable?.dispose()
    }

}

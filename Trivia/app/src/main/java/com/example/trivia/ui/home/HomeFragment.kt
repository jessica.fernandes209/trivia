package com.example.trivia.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trivia.R
import com.example.trivia.data.repository.GameRepository
import kotlinx.android.synthetic.main.view_category_item.view.*
import kotlin.math.absoluteValue

class HomeFragment : Fragment(), HomeView {

    private var presenter: HomePresenter? = null

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.view_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewManager = LinearLayoutManager(this.context)
        viewAdapter = CategoryAdapter(emptyList())

        recyclerView = view.findViewById<RecyclerView>(R.id.home_category_recyclerView).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        view.findViewById<Button>(R.id.home_category_primary).setOnClickListener {
            presenter?.onStartButtonClicked()
        }

        presenter = HomePresenter(this, GameRepository())
    }

    override fun navigateToGame(id: Int) {
        findNavController().navigate(HomeFragmentDirections.actionFirstFragmentToSecondFragment(id))
    }

    override fun setCategoryItems(categoryItems: List<HomeView.CategoryItem>) {
        recyclerView.adapter = CategoryAdapter(categoryItems)
        recyclerView.adapter?.notifyDataSetChanged()
    }

    override fun setStartButtonEnabled() {
        view?.findViewById<Button>(R.id.home_category_primary)?.isEnabled = true
    }

    class CategoryAdapter(private val categories: List<HomeView.CategoryItem>): RecyclerView.Adapter<CategoryViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoryViewHolder(parent)

        override fun getItemCount() = categories.size

        override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
            holder.setData(categories[position])
        }
    }

    class CategoryViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.view_category_item, parent, false)
    ) {
        fun setData(item: HomeView.CategoryItem): CategoryViewHolder {
            itemView.categoryItem_radioButton.text = item.title
            itemView.categoryItem_radioButton.isChecked = item.isChecked
            itemView.categoryItem_radioButton.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    item.onCheckedListener.invoke(item.id)
                }
            }
            return this
        }
    }

}

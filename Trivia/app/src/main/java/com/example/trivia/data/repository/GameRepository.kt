package com.example.trivia.data.repository

import com.example.trivia.data.source.network.ApiClient

class GameRepository {

    private val apiClient = ApiClient()

    fun getCategories() = apiClient.getCategories()

    fun getQuestions(amount: Int, id: Int) = apiClient.getQuestions(amount, id)
}